from collections import deque

class Element:
    def __init__(self, value, type):
        self.value = value
        self.type = type


INV_DICT = {
    "+": Element("-", "operator"),
    "-": Element("+", "operator"),
    "*": Element("/", "operator"),
    "/": Element("*", "operator")
}

def is_numeric(element):
    if element.startswith("-"):
        element = element[1:]

    return element.isnumeric()


def to_element(element):
    if is_numeric(element):
        return Element(int(element), "number")
    elif element == "X":
        return Element([], "X")
    else:
        return Element(element, "operator")


def transform_line(line):
    line = line.strip()

    line = line.split()

    return list(map(to_element, line))


def should_calculate(stack):
    if len(stack) < 2:
        return False

    valid_types = {"number", "X"}

    return (stack[-1].type in valid_types) and (stack[-2].type in valid_types)


def to_operations(n1, n2, operator):
    if n1.type == "X":
        n1.value.append((operator, n2))
        return n1
    else:
        if operator.value != "-":
            n2.value.append((operator, n1))
        else:
            n2.value.append((Element("*", "operator"), Element(-1, "number")))
            n2.value.append((Element("+", "operator"), n1))

        return n2


def primitive_operation(n1, n2, operator):
    if operator.value == "+":
        return Element(n1.value + n2.value, "number")
    elif operator.value == "-":
        return Element(n1.value - n2.value, "number")
    elif operator.value == "*":
        return Element(n1.value * n2.value, "number")
    elif operator.value == "/":
        if n1.value % n2.value != 0:
            return "Err"
        else:
            return Element(n1.value // n2.value, "number")


def perform_operation(n1, n2, operator):
    if n1.type == "X" or n2.type == "X":
        result = to_operations(n1, n2, operator)
        return result
    else:
        return primitive_operation(n1, n2, operator)


def do_inversion(y, operator, n):
    return primitive_operation(y, n, INV_DICT[operator.value])


def invert(y, x):
    for operator, n in reversed(x.value):
        y = do_inversion(y, operator, n)
        if y == "Err":
            return "Err"

    return str(y.value)


def calculate(elements):
    stack = deque()

    y = elements[0]

    for el in elements[1:]:
        stack.append(el)

        while should_calculate(stack):
            op2 = stack.pop()
            op1 = stack.pop()
            operator = stack.pop()

            stack.append(perform_operation(op1, op2, operator))

    assert len(stack) == 1, "STACK LEN AT THE END WASN'T 1"

    return invert(y, stack.pop())


if __name__ == '__main__':
    with open("input.txt", "r") as f:
        read_lines = f.readlines()

        lines = list(map(transform_line, read_lines))

    output = list()
    for line in lines:
        result_str = calculate(line)
        output.append(result_str)

    with open("output.txt", "w") as f:
        f.writelines(" ".join(output))
        f.write("\n")
